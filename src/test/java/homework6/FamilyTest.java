package homework6;

import org.hamcrest.core.StringContains;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class FamilyTest {
        @Test
        public void testToString() {
                Human mother = new Human("Carrot","Olsen",1994);
                Human father = new Human("Bucks","Olsen",1990);

                Family temp = new Family(mother,father);

                assertThat(temp.toString(), containsString("mother=Human{name='Carrot'"));
                assertThat(temp.toString(), containsString("father=Human{name='Bucks'"));
        }

        @Test
        public void addChild() {
                Human mother = new Human("Carrot","Olsen",1994);
                Human father = new Human("Bucks","Olsen",1990);

                Family temp = new Family(mother,father);

                Human child = new Human("Julian","Olsen",2002,88);
                child.addToSchedule(DayOfWeek.FRIDAY,"Tech Academy");
                child.addToSchedule(DayOfWeek.SATURDAY,"Mud Volcano");

                temp.addChild(child);
                assertThat(child, isIn(temp.getChildren()));

                Human child2 = new Human("Curly","Olsen",2005,92);
                child2.addToSchedule(DayOfWeek.FRIDAY,"Attend school");
                child2.addToSchedule(DayOfWeek.SATURDAY,"Volleyball match");

                temp.addChild(child2);
                assertThat(child2, isIn(temp.getChildren()));
        }

        @Test
        public void deleteChild() {
                Human mother = new Human("Carrot","Olsen",1994);
                Human father = new Human("Bucks","Olsen",1990);

                Family temp = new Family(mother,father);

                Human child = new Human("Julian","Olsen",2002,88);
                child.addToSchedule(DayOfWeek.FRIDAY,"Tech Academy");
                child.addToSchedule(DayOfWeek.SATURDAY,"Mud Volcano");
                temp.addChild(child);

                Human child2 = new Human("Curly","Olsen",2005,92);
                child2.addToSchedule(DayOfWeek.FRIDAY,"Attend school");
                child2.addToSchedule(DayOfWeek.SATURDAY,"Volleyball match");
                temp.addChild(child2);

                assertThat(temp.deleteChild(child),equalTo(true));
                assertThat(temp.findChild(child),equalTo(false));
                assertThat(temp.deleteChild(child),equalTo(false));
                assertThat(child, not(isIn((temp.getChildren()))));
                assertThat(child2, (isIn((temp.getChildren()))));
                assertThat(temp.deleteChild(child2),equalTo(true));
        }

        @Test
        public void testDeleteChild() {
                Human mother = new Human("Carrot","Olsen",1994);
                Human father = new Human("Bucks","Olsen",1990);

                Family temp = new Family(mother,father);

                Human child = new Human("Julian","Olsen",2002,88);
                child.addToSchedule(DayOfWeek.FRIDAY,"Tech Academy");
                child.addToSchedule(DayOfWeek.SATURDAY,"Mud Volcano");
                temp.addChild(child);

                Human child2 = new Human("Curly","Olsen",2005,92);
                child2.addToSchedule(DayOfWeek.FRIDAY,"Attend school");
                child2.addToSchedule(DayOfWeek.SATURDAY,"Volleyball match");
                temp.addChild(child2);

                assertThat(temp.deleteChild(0),equalTo(true));
                assertThat(child, not(isIn((temp.getChildren()))));
                assertThat(child2, (isIn((temp.getChildren()))));
                assertThat(temp.deleteChild(0),equalTo(true));
                assertThat(temp.deleteChild(0),equalTo(false));
        }

        @Test
        public void countFamily() {
                Human mother = new Human("Carrot","Olsen",1994);
                Human father = new Human("Bucks","Olsen",1990);

                Family temp = new Family(mother,father);

                Human child = new Human("Julian","Olsen",2002,88);
                child.addToSchedule(DayOfWeek.FRIDAY,"Tech Academy");
                child.addToSchedule(DayOfWeek.SATURDAY,"Mud Volcano");
                temp.addChild(child);

                Human child2 = new Human("Curly","Olsen",2005,92);
                child2.addToSchedule(DayOfWeek.FRIDAY,"Attend school");
                child2.addToSchedule(DayOfWeek.SATURDAY,"Volleyball match");
                temp.addChild(child2);

                assertThat(temp.countFamily(),equalTo(4));
                temp.deleteChild(0);
                assertThat(temp.countFamily(),equalTo(3));
                temp.deleteChild(0);
                assertThat(temp.countFamily(),equalTo(2));
        }
}
