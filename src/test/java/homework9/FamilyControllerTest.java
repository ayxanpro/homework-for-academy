package homework9;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class FamilyControllerTest {
        FamilyController controller;
        Human mother;
        Human father;
        @Before
        public void setUp() throws Exception {
                controller = new FamilyController();
                mother = new Woman("Emma", "Kind", 2001, 85);
                father = new Woman("James", "Kind", 1998, 79);
        }

        @Test
        public void getAllFamilies() {
                controller.createNewFamily(mother,father);
                Family expected = new Family(mother,father);
                assertThat(controller.getAllFamilies().get(0), equalTo(expected));
        }

        @Test
        public void getFamiliesBiggerThan() {
                controller.createNewFamily(mother,father);
                Family expected = new Family(mother,father);
                assertThat(controller.getFamiliesBiggerThan(1).get(0), equalTo(expected));
        }

        @Test
        public void getFamiliesLessThan() {
                controller.createNewFamily(mother,father);
                Family expected = new Family(mother,father);
                assertThat(controller.getFamiliesLessThan(3).get(0), equalTo(expected));
        }

        @Test
        public void countFamiliesWithMemberNumber() {
                controller.createNewFamily(mother,father);
                assertThat(controller.countFamiliesWithMemberNumber(2), equalTo(1));
        }

        @Test
        public void deleteFamilyByIndex() {
                controller.createNewFamily(mother,father);
                assertThat(controller.countFamiliesWithMemberNumber(2), equalTo(1));
                controller.deleteFamilyByIndex(0);
                assertThat(controller.getAllFamilies().size(), equalTo(0));
        }

        @Test
        public void bornChild() {
                controller.createNewFamily(mother,father);
                Family fam = new Family(mother,father);
                controller.bornChild(fam, "Kent", "Jenny");
                assertThat(controller.getAllFamilies().get(0).countFamily(), equalTo(3));
        }

        @Test
        public void adoptChild() {
                controller.createNewFamily(mother,father);
                Family fam = new Family(mother,father);
                Human child = new Human("Jelly", mother.getSurname(), 2005, 88);
                controller.adoptChild(fam, child);
                assertThat(controller.getAllFamilies().get(0).countFamily(), equalTo(3));
        }

        @Test
        public void deleteAllChildrenOlderThen() {
                controller.createNewFamily(mother,father);
                Family fam = new Family(mother,father);
                Human child = new Human("Jelly", mother.getSurname(), 2005, 88);
                controller.adoptChild(fam, child);
                controller.deleteAllChildrenOlderThen(11);
                assertThat(controller.getAllFamilies().get(0).countFamily(), equalTo(2));
        }

        @Test
        public void count() {
                controller.createNewFamily(mother,father);
                Family fam = new Family(mother,father);
                Human child = new Human("Jelly", mother.getSurname(), 2005, 88);
                controller.adoptChild(fam, child);
                assertThat(controller.count(), equalTo(1));
        }

        @Test
        public void getFamilyById() {
                controller.createNewFamily(mother,father);
                Family fam = new Family(mother,father);
                Human child = new Human("Jelly", mother.getSurname(), 2005, 88);
                controller.adoptChild(fam, child);
                assertThat(controller.getFamilyById(0), equalTo(fam));
        }

}