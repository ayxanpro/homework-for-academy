package homework8;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class FamilyTest {
        public Family fam;
        @Before
        public void setUp() throws Exception {
                Human jen = new Woman("Jennifer", "Deamonid", 2002, 88);
                Human an = new Man("Anglen", "Deamonid", 2000, 89);
                Pet voody = new Dog("voody");

                fam = new Family(jen,an);
                fam.addPet(voody);
        }

        @Test
        public void testToString() {
                assertThat(fam.toString(),containsString("Jennifer"));
                assertThat(fam.toString(),containsString("Anglen"));
                assertThat(fam.toString(),containsString("voody"));
        }

        @Test
        public void makeChild() {
                Human newChild = ((Woman)fam.getMother()).bornChild(fam.getFather());

                assertThat(newChild.toString(),containsString("Deamonid"));
                assertThat(newChild.getYear(),greaterThanOrEqualTo(LocalDate.now().getYear()));
        }
        @Test
        public void addChild() {
                assertThat(fam.countFamily(),equalTo(2));

                Human newChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(newChild);
                assertThat(fam.countFamily(),equalTo(3));

                Human secondChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(secondChild);
                assertThat(fam.countFamily(),equalTo(4));
        }

        @Test
        public void deleteChild() {
                assertThat(fam.countFamily(),equalTo(2));
                Human newChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(newChild);
                Human secondChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(secondChild);

                assertThat(fam.countFamily(),equalTo(4));
                assertThat(fam.deleteChild(1),equalTo(true));
                assertThat(fam.countFamily(),equalTo(3));
                assertThat(fam.deleteChild(0),equalTo(true));
                assertThat(fam.countFamily(),equalTo(2));
        }

        @Test
        public void testDeleteChild() {
                assertThat(fam.countFamily(),equalTo(2));
                Human newChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(newChild);
                Human secondChild = ((Woman)fam.getMother()).bornChild(fam.getFather());
                fam.addChild(secondChild);

                assertThat(fam.countFamily(),equalTo(4));
                assertThat(fam.deleteChild(newChild),equalTo(true));
                assertThat(fam.countFamily(),equalTo(3));
        }

        @Test
        public void countFamily() {
        }
}