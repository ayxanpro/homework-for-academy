package homework4;

public class Family {

  Human mother;
  Human father;
  Human child;
  Pet pet;

  public Family(Human mother, Human father, Human child, Pet pet) {
    this.mother = mother;
    this.father = father;
    this.child = child;
    this.pet = pet;
    this.child.mother = this.mother;
    this.child.father = this.father;
    this.child.pet = this.pet;
  }

  public Family() {
  }
}
