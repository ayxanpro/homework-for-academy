package homework4;

public class Human {
  String name;
  String surname;
  int year;
  int iq;
  Pet pet;
  Human mother;
  Human father;
  String[][] schedule;

  public Human(String name, String surname, int year) {
    this(name, surname, year, null, null);
  }

  public Human(String name, String surname, int year, Human mother, Human father) {
    this(name, surname, year, 0, null, mother, father, null);
  }

  public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
    this.pet = pet;
    this.mother = mother;
    this.father = father;
    this.schedule = schedule;
  }

  public Human() {
    this("unknown name(", "unknown surname", -1);
  }

  public void greetPet() {
    System.out.printf("Hello, %s\n", this.pet.nickname);
  }

  public void describePet() {
    System.out.printf("I have a %s," +
            " he is %d years old," +
            " he is %s\n",
        this.pet.species,
        this.pet.age,
        this.pet.isHeSly());
  }

  @Override
  public String toString() {
    return String.format("Human{name='%s'," +
            " surname='%s'," +
            " year=%d, iq=%d," +
            " mother=%s, " +
            "father=%s," +
            " pet=%s," +
            "habits=%s}",
        name,
        surname,
        year,
        iq,
        mother.fullName(),
        father.fullName(),
        pet.toString(),
        (schedule != null ? habitsToString() : "no habits"));
  }

  private String fullName() {
    return String.format("%s %s", this.name, this.surname);
  }


  private String habitsToString() {
    String res = "";
    for (int i = 0; i < schedule[0].length; i++) {
      if (schedule[0][i] != null) {
        res += String.format("%s: %s", schedule[0][i], schedule[1][i]);
      }
    }
    return res;
  }
}
