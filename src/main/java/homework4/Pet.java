package homework4;

import java.util.Arrays;

public class Pet {
  String species;
  String nickname;
  int age;
  int trickLevel;
  String[] habits;

  public Pet(String species, String nickname) {
    this(species, nickname, 0, 0, null);
  }

  public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
    this.species = species;
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Pet() {
    this("(it is something;)", "creatio");
  }

  public void eat() {
    System.out.println("I am eating");
  }

  public void respond() {
    System.out.printf("Hello, owner. I am -%s. I miss you!\n", this.nickname);
  }

  public void foul() {
    System.out.println("I need to cover it up");
  }

  @Override
  public String toString() {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
        species, nickname, age, trickLevel,
        (habits != null ? Arrays.toString(habits) : "no habits"));
  }

  public String isHeSly() {
    return this.trickLevel > 50 ? "very sly"
        : "not so sly";
  }
}
