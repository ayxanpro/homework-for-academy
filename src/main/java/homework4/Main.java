package homework4;

public class Main {
  //NOTE Family class used in this example is not any how related with the homework5
  public static void main(String[] args) {

    Family family1 = new Family(new Human("Sarah", "Jane", 1992),
        new Human("Patrick", "Jane", 1988),
        new Human("Liz", "Jane", 2002),
        new Pet("cat", "Pinky"));

    Family family2 = new Family(new Human("Mizzy", "Smith", 1995),
        new Human(),
        new Human("July", "Smith", 2007),
        new Pet("dog", "Bolt", 3, 52, new String[]{"fetching ball", "barking at cats"}));
    //Third family
    Human grandfather = new Human("Barry", "Allen", 1994);
    Human grandmother = new Human("Caty", "Allen", 1994);
    Human mother3 = new Human("Carrot", "Olsen", 1194, grandmother, grandfather);
    Human father3 = new Human();
    Pet pet3 = new Pet();
    Human child = new Human("Julian", "Olsen", 2002, 88,
        pet3, mother3, father3,
        new String[][]{{"Friday", "Saturday"}, {"Tech Academy", "Mud Volcano"}});
    Family family3 = new Family(mother3, father3, child, pet3);

    family1.child.greetPet();
    family1.child.describePet();
    System.out.println(family1.child);

    family1.pet.respond();
    family1.pet.eat();
    family1.pet.foul();
    System.out.println(family1.pet);

    System.out.println("---------------------");

    family2.child.greetPet();
    family2.child.describePet();
    System.out.println(family2.child);

    family2.pet.respond();
    family2.pet.eat();
    family2.pet.foul();
    System.out.println(family2.pet);

    System.out.println("---------------------");

    family3.child.greetPet();
    family3.child.describePet();
    System.out.println(family3.child);

    family3.pet.respond();
    family3.pet.eat();
    family3.pet.foul();
    System.out.println(family3.pet);
  }
}
