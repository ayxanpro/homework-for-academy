package homework3;

import java.util.Scanner;

public class TaskPlanner {
  public static int getDay(String day) {
    int res = -1;
    if (day.equalsIgnoreCase("monday")) {
      res = 1;
    } else if (day.equalsIgnoreCase("tuesday")) {
      res = 2;
    } else if (day.equalsIgnoreCase("wednesday")) {
      res = 3;
    } else if (day.equalsIgnoreCase("thursday")) {
      res = 4;
    } else if (day.equalsIgnoreCase("friday")) {
      res = 5;
    } else if (day.equalsIgnoreCase("saturday")) {
      res = 6;
    } else if (day.equalsIgnoreCase("sunday")) {
      res = 0;
    }
    return res;
  }

  public static String capitalize(String a) {
    String first = a.substring(0, 1).toUpperCase();
    return first + a.substring(1);
  }

  public static void main(String[] args) {
    //initialize
    Scanner scan = new Scanner(System.in);
    String[][] scedule = new String[7][2];
    //fill with values
    scedule[0][0] = "Sunday";
    scedule[0][1] = "do home work";
    scedule[1][0] = "Monday";
    scedule[1][1] = "go to courses; watch a film";
    //start infinite loop
    for (; ; ) {
      //get user input
      System.out.println("Please, input the day of the week:");
      String ans = scan.nextLine();
      if (ans.split(" ").length > 1)//check if input contains more than one word
      {
        String operator = ans.split(" ")[0];
        String day = ans.split(" ")[1];
        if (getDay(day) != -1)//if input is valid day of week
        {
          if (operator.equalsIgnoreCase("change")
              || operator.equalsIgnoreCase("reschedule ")) {
            int numberOfDay = getDay(day);
            if (scedule[numberOfDay][0] == null) {
              scedule[numberOfDay][0] = capitalize(day);
            }
            System.out.printf("Please, input new tasks for %s.", scedule[numberOfDay][0]);
            String newTask = scan.nextLine();
            scedule[numberOfDay][1] = newTask;
          }
        } else {
          System.out.println(" Sorry, I don't understand you, please try again.");
        }
      } else if (ans.equalsIgnoreCase("exit")) {//if input is exit
        break;
      } else if (getDay(ans) != -1)//if it is only day of week then print task
      {
        int day = getDay(ans);
        if (scedule[day][1] != null)
          System.out.printf("Your task for %s:%s\n", scedule[day][0], scedule[day][1]);
        else {
          System.out.println("You don't have any plans for that day");
        }
      } else {//if not exit
        System.out.println(" Sorry, I don't understand you, please try again.");
      }
    }
  }
}
