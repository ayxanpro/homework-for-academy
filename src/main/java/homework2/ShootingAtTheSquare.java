package homework2;

import java.util.Random;
import java.util.Scanner;

public class ShootingAtTheSquare {
  public static void main(String[] args) {
    //Declaring the grid
    int[][] grid = new int[5][5];
    Random r = new Random();
    Scanner scan = new Scanner(System.in);
    //Setting the target randomly

    grid[r.nextInt(5)][r.nextInt(5)] = 1;
    System.out.println("All set. Get ready to rumble!");
    //Starting the infinite loop
    while (true) {
      //Getting the line number from the user
      int line;
      do {
        System.out.println("Please enter the line to shoot: ");
        line = scan.nextInt();
      } while (line < 1 || line > 5);
      line--;
      //Getting bar number from the user
      int bar;
      do {
        System.out.println("Please enter the bar to shoot: ");
        bar = scan.nextInt();
      } while (bar < 1 || bar > 5);
      bar--;
      //checking if the user hit the target
      if (grid[line][bar] != 1) {
        grid[line][bar] = 2;
      }
      //Printing the GRID
      for (int i = 0; i <= 5; i++) {
        for (int j = 0; j <= 5; j++) {
          if (grid[line][bar] == 1 && line == i - 1 && bar == j - 1) {
            System.out.print("x |");
          } else if (j == 0) {
            System.out.print(i + " | ");
          } else if (i == 0) {
            System.out.print(j + " | ");
          } else if (grid[i - 1][j - 1] != 2) {
            System.out.print("- | ");
          } else if (grid[i - 1][j - 1] == 2) {
            System.out.print("* | ");
          }
        }
        System.out.println();
      }
      if (grid[line][bar] == 1) {
        System.out.println("You have won!");
        break;
      }
    }
  }
}
