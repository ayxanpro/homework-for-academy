package homework6;

public class Main {
  public static void main(String[] args) {
    Human grandfather = new Human("Barry", "Allen", 1972);
    Human grandmother = new Human("Caty", "Allen", 1974);
    Human mother = new Human("Carrot", "Olsen", 1994);

    Family grands = new Family(grandmother, grandfather);

    System.out.println(grands);
    grands.addChild(mother);
    System.out.println(grands);

    Human father = new Human();
    father.setName("Bucks");
    father.setSurname("Olsen");
    father.setYear(1990);

    Pet parrot = new Pet(Species.PARROT, "Boocky");

    Human child = new Human("Julian", "Olsen", 2002, 88);
    child.addToSchedule(DayOfWeek.FRIDAY, "Tech Academy");
    child.addToSchedule(DayOfWeek.SATURDAY, "Mud Volcano");

    Human child2 = new Human("Curly", "Olsen", 2005, 92);
    child2.addToSchedule(DayOfWeek.FRIDAY, "Attend school");
    child2.addToSchedule(DayOfWeek.SATURDAY, "Volleyball match");

    Family ours = new Family(mother, father);
    ours.setPet(parrot);

    System.out.println(ours);
    System.out.println("Number of members:" + ours.countFamily());

    ours.addChild(child);
    ours.addChild(child2);
    System.out.println(ours);
    System.out.println("Number of members:" + ours.countFamily());

    ours.deleteChild(0);
    System.out.println(ours);
    System.out.println("Number of members:" + ours.countFamily());

    ours.addChild(child);
    System.out.println(ours);
    System.out.println("Number of members:" + ours.countFamily());

    ours.deleteChild(child2);
    ours.deleteChild(child);

    System.out.println(ours);
    System.out.println("Number of members:" + ours.countFamily());
  }
}
