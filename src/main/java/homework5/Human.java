package homework5;

public class Human {
  private String name;
  private String surname;
  private int year;
  private int iq;
  private Family family;
  private String[][] schedule;

  public Human(String name, String surname, int year) {
    this(name, surname, year, 0, null);
  }

  public Human(String name, String surname, int year, int iq, String[][] schedule) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
    this.schedule = schedule;
  }

  public Human() {
    this("unknown name(", "unknown surname", -1);
  }

  public void greetPet() {
    System.out.printf("Hello, %s\n", this.family.getPet().getNickname());
  }

  public void describePet() {
    System.out.printf("I have a %s," +
            " he is %d years old," +
            " he is %s\n",
        this.family.getPet().getSpecies(),
        this.family.getPet().getAge(),
        this.family.getPet().isHeSly());
  }

  @Override
  public String toString() {
    return String.format("Human{name='%s'," +
            " surname='%s'," +
            " year=%d, iq=%d," +
            "habits=%s}",
        name,
        surname,
        year,
        iq,
        (schedule != null ? habitsToString() : "no habits"));
  }


  private String habitsToString() {
    if (schedule == null) return "";
    String res = "[";
    for (int i = 0; i < schedule.length; i++) {
      res += String.format("[%s, %s]", schedule[i][0], schedule[i][1]);
    }
    res += "]";
    return res;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) return true;
    if (!(obj instanceof Human)) return false;

    Human that = (Human) obj;

    if (this.name != that.getName()) return false;
    if (this.surname != that.getSurname()) return false;
    if (this.year != that.getYear()) return false;
    if (this.iq != that.getIq()) return false;

    return true;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    this.iq = iq;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String[][] getSchedule() {
    return schedule;
  }

  public void setSchedule(String[][] schedule) {
    this.schedule = schedule;
  }

}
