package homework5;

import java.util.Arrays;

public class Family {

  private Human mother;
  private Human father;
  private Human[] children;
  private Pet pet;

  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;
    this.children = new Human[0];
  }

  @Override
  public String toString() {
    return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}",
        mother, father, getChildrenInfo(), pet);
  }

  public void addChild(Human child) {
    child.setFamily(this);
    children = Arrays.copyOf(children, children.length + 1);
    children[children.length - 1] = child;
  }

  public boolean deleteChild(int index) {
    Human temp[] = new Human[children.length - 1];
    int counter = 0;
    children[index].setFamily(null);
    for (int i = 0; i < children.length; i++) {
      if (i != index)
        temp[counter++] = children[i];
    }
    children = temp;
    return index >= 0 && index < children.length;
  }

  public int countFamily() {
    return 2 + children.length;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Family)) return false;
    Family b = (Family) obj;

    if (b.father.equals(this.father) &&
        b.mother.equals(this.mother)) return true;
    return false;
  }

  private String getChildrenInfo() {
    String res = "";
    for (Human child : children) {
      res += child.toString();
    }
    return res == "" ? "no children" : res;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Human[] getChildren() {
    return children;
  }

  public void setChildren(Human[] child) {
    this.children = child;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }
}
