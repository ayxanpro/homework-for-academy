package homework5;

import java.util.Arrays;

public class Pet {
  private String species;
  private String nickname;
  private int age;
  private int trickLevel;


  private String[] habits;

  public Pet(String species, String nickname) {
    this(species, nickname, 0, 0, null);
  }

  public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
    this.species = species;
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Pet() {
    this("(it is something;)", "creatio");
  }

  public void eat() {
    System.out.println("I am eating");
  }

  public void respond() {
    System.out.printf("Hello, owner. I am -%s. I miss you!\n", this.nickname);
  }

  public void foul() {
    System.out.println("I need to cover it up");
  }

  @Override
  public String toString() {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
        species, nickname, age, trickLevel,
        (habits != null ? Arrays.toString(habits) : "no habits"));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Pet)) return false;

    Pet that = (Pet) obj;

    if (this.species != that.getSpecies()) return false;
    if (this.nickname != that.getNickname()) return false;
    if (this.age != that.getAge()) return false;
    if (this.trickLevel != that.getTrickLevel()) return false;

    return true;
  }


  public String isHeSly() {
    return this.trickLevel > 50 ? "very sly"
        : "not so sly";
  }

  public String getSpecies() {
    return species;
  }

  public void setSpecies(String species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    this.trickLevel = trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void setHabits(String[] habits) {
    this.habits = habits;
  }

}
