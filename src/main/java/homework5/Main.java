package homework5;

public class Main {
  public static void main(String[] args) {
    Human grandfather = new Human("Barry", "Allen", 1992);
    Human grandmother = new Human("Caty", "Allen", 1994);
    Human mother = new Human("Carrot", "Olsen", 1194);

    Family grands = new Family(grandmother, grandfather);

    System.out.println(grands);
    grands.addChild(mother);
    System.out.println(grands);

    Human father = new Human();
    father.setName("Bucks");
    father.setSurname("Olsen");
    father.setYear(1990);

    Pet parrot = new Pet("parrot", "Boocky");
    Human child = new Human("Julian", "Olsen", 2002, 88,
        new String[][]{{"Friday", "Tech Academy"}, {"Saturday", "Mud Volcano"}});
    Family ours = new Family(mother, father);
    ours.setPet(parrot);
    System.out.println(ours);
    ours.addChild(child);
    System.out.println(ours);

    ours.deleteChild(0);
    System.out.println(ours);
  }
}
