package homework9;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao, Iterable<Family> {
  List<Family> families = new ArrayList<>();

  @Override
  public List getAllFamilies() {
    return families;
  }

  @Override
  public Object getFamilyByIndex(int i) {
    return i < families.size() ? families.get(i) : null;
  }

  @Override
  public boolean deleteFamily(int index) {
    return index < families.size() && (families.remove(index) != null);
  }

  @Override
  public boolean deleteFamily(Object fam) {
    if (!(fam instanceof Family)) return false;
    return families.remove((Family) fam);
  }

  @Override
  public void saveFamily(Object fam) {
    if (!(fam instanceof Family)) return;
    if (families.contains((Family) fam)) {
      int i = families.indexOf(fam);
      families.set(i, (Family) fam);
    } else {
      families.add((Family) fam);
    }
  }

  @Override
  public int count() {
    return families.size();
  }

  @Override
  public Iterator iterator() {
    return families.iterator();
  }
}
