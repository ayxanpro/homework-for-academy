package homework9;

public class Main {
  public static void main(String[] args) {
    FamilyController fc = new FamilyController();
    Human mother = new Woman("Emma", "Kind", 2001, 85);
    Human father = new Woman("James", "Kind", 1998, 79);

    fc.createNewFamily(mother, father);

    System.out.println(fc.getAllFamilies());
    System.out.println("--------------------------");
    fc.displayAllFamilies();
    System.out.println("--------------------------");
    Family test = fc.getFamilyById(0);

    System.out.println(test);
    System.out.println("--------------------------");
    fc.bornChild(test, "Oliver", "Olivia");
    System.out.println(fc.getFamiliesBiggerThan(2));
    System.out.println("--------------------------");
    fc.deleteAllChildrenOlderThen(-1);
    System.out.println(fc.getAllFamilies());
    System.out.println("--------------------------");
    System.out.println(fc.getFamiliesLessThan(3));
    System.out.println("--------------------------");
  }
}
