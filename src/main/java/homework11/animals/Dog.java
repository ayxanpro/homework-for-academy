package homework11.animals;

import java.util.Set;

public class Dog extends Pet implements Foulable {
  public Dog(String nickname) {
    this(nickname, 1, 99, null);
  }

  public Dog(String nickname, int age, int trickLevel, Set habits) {
    super(nickname, age, trickLevel, habits);
  }

  public Dog() {
    this("creatio");
  }

  @Override
  public void foul() {
    System.out.println("I need to cover it up");
  }

  @Override
  public void respond() {
    System.out.println("hav,hav");
  }
}
