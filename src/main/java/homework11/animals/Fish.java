package homework11.animals;

import java.util.Set;

public class Fish extends Pet {
  public Fish(String nickname) {
    this(nickname, 1, 99, null);
  }

  public Fish(String nickname, int age, int trickLevel, Set habits) {
    super(nickname, age, trickLevel, habits);
  }

  public Fish() {
    this("creatio");
  }

  @Override
  public void respond() {
    System.out.println("bul,bul,bul");
  }
}
