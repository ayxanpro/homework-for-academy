package homework11.animals;

public enum Species {
  DOMESTIC_CAT, DOG, FISH, ROBOCAT, UNKNOWN
}
