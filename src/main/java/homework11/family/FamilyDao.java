package homework11.family;

import java.util.List;

public interface FamilyDao<Family> {
  List<Family> getAllFamilies();

  Family getFamilyByIndex(int i);

  boolean deleteFamily(int index);

  boolean deleteFamily(Family fam);

  void saveFamily(Family fam);

  int count();
}
