package homework11.family;

import homework11.animals.Pet;
import homework11.human.Human;

import java.util.List;

public class FamilyController {
  FamilyService service = new FamilyService();

  List<Family> getAllFamilies() {
    return service.getAllFamilies();
  }

  void displayAllFamilies() {
    service.displayAllFamilies();
  }

  List getFamiliesBiggerThan(int min) {
    return service.getFamiliesBiggerThan(min);
  }

  List getFamiliesLessThan(int max) {
    return service.getFamiliesLessThan(max);
  }

  int countFamiliesWithMemberNumber(int eq) {
    return service.countFamiliesWithMemberNumber(eq);
  }

  void createNewFamily(Human mother, Human father) {
    service.createNewFamily(mother, father);
  }

  void deleteFamilyByIndex(int index) {
    service.deleteFamilyByIndex(index);
  }

  void bornChild(Family fam, String boy, String girl) {
    service.bornChild(fam, boy, girl);
  }

  void adoptChild(Family fam, Human child) {
    service.adoptChild(fam, child);
  }

  void deleteAllChildrenOlderThen(int max_age) {
    service.deleteAllChildrenOlderThen(max_age);
  }

  int count() {
    return service.count();
  }

  Family getFamilyById(int id) {
    return service.getFamilyById(id);
  }

  List getPets(int id) {
    return service.getPets(id);
  }

  void addPet(int id, Pet pet) {
    service.addPet(id, pet);
  }
}
