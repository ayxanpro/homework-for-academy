package homework11.family;

import homework11.animals.Pet;
import homework11.human.Human;
import homework11.human.Man;
import homework11.human.Woman;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
  private CollectionFamilyDao familyDao;

  public FamilyService() {
    familyDao = new CollectionFamilyDao();
  }

  List<Family> getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  void displayAllFamilies() {
    familyDao.forEach(System.out::println);
  }

  List<Family> getFamiliesBiggerThan(int min) {
    return familyDao.getAllFamilies().stream()
        .filter(fam -> fam.countFamily() > min)
        .collect(Collectors.toList());
  }

  List getFamiliesLessThan(int max) {
    return familyDao.getAllFamilies().stream()
        .filter(fam -> fam.countFamily() < max)
        .collect(Collectors.toList());
  }

  int countFamiliesWithMemberNumber(int eq) {
    return (int) familyDao.getAllFamilies().stream()
        .filter(fam -> fam.countFamily() == eq)
        .count();
  }

  void createNewFamily(Human mother, Human father) {
    Family fam = new Family(mother, father);
    familyDao.saveFamily(fam);
  }

  void deleteFamilyByIndex(int index) {
    familyDao.deleteFamily(index);
  }

  void bornChild(Family fam, String boy, String girl) {
    Human child = ((Woman) fam.getMother()).bornChild(fam.getFather());
    if (child instanceof Man) {
      child.setName(boy);
    } else {
      child.setName(girl);
    }
    fam.adoptChild(child);
    familyDao.saveFamily(fam);
  }

  void adoptChild(Family fam, Human child) {
    fam.adoptChild(child);
    familyDao.saveFamily(fam);
  }

  void deleteAllChildrenOlderThen(int max_age) {

    getAllFamilies().forEach(fam -> {
      fam.setChildren(
          fam.getChildren()
              .stream()
              .filter(child ->
                  LocalDate.now().getYear() - child.getBirthDate() < max_age)
              .collect(Collectors.toList()));

      familyDao.saveFamily(fam);
    });

  }

  int count() {
    return familyDao.count();
  }

  Family getFamilyById(int id) {
    return familyDao.getFamilyByIndex(id);
  }

  List getPets(int id) {
    return (List) familyDao.getFamilyByIndex(id).getPet();
  }

  void addPet(int id, Pet pet) {
    Family fam = familyDao.getFamilyByIndex(id);
    fam.addPet(pet);
    familyDao.saveFamily(fam);
  }
}
