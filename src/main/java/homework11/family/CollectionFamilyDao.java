package homework11.family;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao<Family>, Iterable<Family> {

  private List<Family> families = new ArrayList<>();

  @Override
  public List<Family> getAllFamilies() {
    return families;
  }

  @Override
  public Family getFamilyByIndex(int i) {
    return i < families.size() ? families.get(i) : null;
  }

  @Override
  public boolean deleteFamily(int index) {
    return index < families.size() && (families.remove(index) != null);
  }

  @Override
  public boolean deleteFamily(Family fam) {
    return families.remove(fam);
  }

  @Override
  public void saveFamily(Family fam) {
    if (families.contains(fam)) {
      int i = families.indexOf(fam);
      families.set(i, fam);
    } else {
      families.add(fam);
    }
  }

  @Override
  public int count() {
    return families.size();
  }

  @Override
  public Iterator iterator() {
    return families.iterator();
  }
}
