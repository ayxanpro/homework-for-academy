package homework11.family;

import homework11.animals.Pet;
import homework11.human.Human;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Family {

  private Human mother;
  private Human father;
  private List<Human> children;
  private Set<Pet> pet;

  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;

    this.mother.setFamily(this);
    this.father.setFamily(this);

    this.children = new ArrayList<>();
    this.pet = new HashSet<>();
  }

  @Override
  public String toString() {
    return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}",
        mother, father, getChildrenInfo(), getPetsInfo());
  }

  public void adoptChild(Human child) {
    if (this.findChild(child)) return;
    child.setFamily(this);
    children.add(child);
  }

  public void adoptChild(String name, String surname, String date, int iq) {
    SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
    try {
      Date d = sf.parse(date);
      Human child = new Human(name, surname, d.getTime(), iq);
      if (this.findChild(child)) return;
      child.setFamily(this);
      children.add(child);
    } catch (ParseException e) {
      System.out.println("something went wrong");
    }

  }

  public boolean deleteChild(int index) {
    if (index > children.size() || index < 0) return false;
    if (children.size() == 0) return false;
    children.remove(index);
    return true;
  }

  public boolean deleteChild(Human child) {
    return children.remove(child);
  }

  public void addPet(Pet pet) {
    this.pet.add(pet);
  }

  public boolean findChild(Human child_) {
    for (Human child : children) {
      if (child.equals(child_)) {
        System.out.println(child);
        return true;
      }
    }
    return false;
  }

  public int countFamily() {
    return 2 + children.size();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Family)) return false;
    Family b = (Family) obj;

    return b.father.equals(this.father) &&
        b.mother.equals(this.mother);
  }

  private String getChildrenInfo() {
    String res = "";
    for (Human child : children) {
      res += child.toString();
    }
    return res == "" ? "no children" : res;
  }

  public String getPetsInfo() {
    String res = "";
    for (Pet p : pet) {
      res += p.toString();
    }
    return res == "" ? "no children" : res;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public ArrayList<Human> getChildren() {
    return (ArrayList<Human>) children;
  }

  public void setChildren(List<Human> children) {
    this.children = children;
  }

  public Set<Pet> getPet() {
    return pet;
  }

  public void setPet(Set<Pet> pet) {
    this.pet = pet;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Family has collapsed!!!");
  }
}
