package homework10.animals;

public enum Species {
  DOMESTIC_CAT, DOG, FISH, ROBOCAT, UNKNOWN;
}
