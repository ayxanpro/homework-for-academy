package homework10.family;

import homework10.animals.Pet;
import homework10.human.Human;
import homework10.human.Man;
import homework10.human.Woman;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FamilyService {
  private FamilyDao familyDao;

  public FamilyService() {
    familyDao = new CollectionFamilyDao();
  }

  List getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  void displayAllFamilies() {
    ((CollectionFamilyDao) familyDao).forEach(System.out::println);
  }

  List getFamiliesBiggerThan(int min) {
    List<Family> res = new ArrayList<>();
    for (Family fam : (CollectionFamilyDao) familyDao) {
      if (fam.countFamily() > min) res.add(fam);
    }
    return res;
  }

  List getFamiliesLessThan(int max) {
    List<Family> res = new ArrayList<>();
    for (Family fam : (CollectionFamilyDao) familyDao) {
      if (fam.countFamily() < max) res.add(fam);
    }
    return res;
  }

  int countFamiliesWithMemberNumber(int eq) {
    int res = 0;
    for (Family fam : (CollectionFamilyDao) familyDao) {
      if (fam.countFamily() == eq) res++;
    }
    return res;
  }

  void createNewFamily(Human mother, Human father) {
    Family fam = new Family(mother, father);
    familyDao.saveFamily(fam);
  }

  void deleteFamilyByIndex(int index) {
    familyDao.deleteFamily(index);
  }

  void bornChild(Family fam, String boy, String girl) {
    Human child = ((Woman) fam.getMother()).bornChild(fam.getFather());
    if (child instanceof Man) {
      child.setName(boy);
    } else {
      child.setName(girl);
    }
    fam.adoptChild(child);
    familyDao.saveFamily(fam);
  }

  void adoptChild(Family fam, Human child) {
    fam.adoptChild(child);
    familyDao.saveFamily(fam);
  }

  void deleteAllChildrenOlderThen(int max_age) {
    for (Family fam : (CollectionFamilyDao) familyDao) {
      ArrayList<Human> temp = (ArrayList<Human>) fam.getChildren().clone();
      for (Human child : temp) {
        if (LocalDate.now().getYear() - child.getBirthDate() > max_age) {
          fam.deleteChild(child);
        }
      }
      familyDao.saveFamily(fam);
    }
  }

  int count() {
    return familyDao.count();
  }

  Family getFamilyById(int id) {
    return (Family) familyDao.getFamilyByIndex(id);
  }

  List getPets(int id) {
    return new ArrayList(((Family) familyDao.getFamilyByIndex(id)).getPet());
  }

  void addPet(int id, Pet pet) {
    Family fam = ((Family) familyDao.getFamilyByIndex(id));
    fam.addPet(pet);
    familyDao.saveFamily(fam);
  }
}
