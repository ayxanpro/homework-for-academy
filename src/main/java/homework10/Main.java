package homework10;

import homework10.family.Family;
import homework10.human.Human;
import homework10.human.Woman;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
  public static void main(String[] args) throws ParseException {
    SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");

    Date md = sf.parse("2001/11/24");
    Date fd = sf.parse("1999/01/13");

    Human mother = new Woman("Emma", "Kind", md.getTime(), 85);
    Human father = new Woman("James", "Kind", fd.getTime(), 79);
    Family fam = new Family(mother, father);

    fam.adoptChild("Yellow", "World", "2005/11/14", 77);

    System.out.println(mother.describeAge());
    System.out.println(father.describeAge());
    System.out.println(fam);

  }
}
