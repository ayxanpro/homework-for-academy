package homework10.human;

import homework10.family.Family;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Human {
  private String name;
  private String surname;
  private long birthDate;
  private int iq;
  private Family family;
  private HashMap<DayOfWeek, String> schedule;

  public Human(String name, String surname, long birthDate) {
    this(name, surname, birthDate, 0);
  }

  public Human(String name, String surname, long birthDate, int iq) {
    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
    this.iq = iq;
    fillSchedule();
  }

  public Human() {
    this("unknown name(", "unknown surname", -1);
  }

  public void greetPet() {
    System.out.printf("Hello, %s\n", this.family.getPet().iterator());
  }

  public void describePet() {
    System.out.printf("My family owns:%s\n", this.family.getPetsInfo());
  }

  @Override
  public String toString() {
    return String.format("Human{name='%s'," +
            " surname='%s'," +
            " date of birth=%s, iq=%d," +
            "habits=%s}",
        name,
        surname,
        describeAge(),
        iq,
        (schedule != null ? habitsToString() : "no habits"));
  }

  private String habitsToString() {
    if (schedule.isEmpty()) return "no plans for this week";
    String res = "[";
    for (DayOfWeek day : schedule.keySet()) {
      if (schedule.get(day) != "")
        res += String.format("[%s, %s]", day.name(), schedule.get(day));
    }
    res += "]";
    return res;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) return true;
    if (!(obj instanceof Human)) return false;

    Human that = (Human) obj;

    if (this.name.equalsIgnoreCase(that.getName())) return false;
    if (this.surname.equalsIgnoreCase(that.getSurname())) return false;
    if (this.birthDate != that.getBirthDate()) return false;
    if (this.iq != that.getIq()) return false;

    return true;
  }

  public void fillSchedule() {
    this.schedule = new HashMap<>();
    for (DayOfWeek day : DayOfWeek.values())
      this.schedule.put(day, "");
  }

  public void addToSchedule(DayOfWeek day, String task) {
    schedule.put(day, task);
  }

  public String describeAge() {
    Date d = new Date(birthDate);
    SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
    return sf.format(d);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public long getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(int birthDate) {
    this.birthDate = birthDate;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    this.iq = iq;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public HashMap<DayOfWeek, String> getSchedule() {
    return schedule;
  }

  public void setSchedule(HashMap schedule) {
    this.schedule = schedule;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("R.I.P.");
  }
}
