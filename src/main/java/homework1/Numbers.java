package homework1;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
  public static void main(String[] args) {
    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    //Guessing random number
    System.out.println("ayxan".join("123", "qwe"));
    int answer = rand.nextInt(100);
    int[] userGuesses = new int[100];
    //Getting user name

    System.out.print("Please, enter your name: ");
    String name = scan.nextLine();
    //Printing some message
    System.out.println("Let the game begin!");
    //Starting infinite cycle
    int last = 0;
    while (true) {
      System.out.print("Please enter your guess: ");
      int guess = scan.nextInt();
      userGuesses[last++] = guess;
      if (guess < answer) {
        System.out.println("Your number is too small. Please, try again.");
      } else if (guess > answer) {
        System.out.println("Your number is too big. Please, try again.");
      } else {
        //informing the user
        System.out.printf("Congratulations, %s!\n", name);
        //Printing all user guesses
        System.out.println("Your numbers:");
        //sorting an array
        for (int i = 0; i < last; i++)
          for (int j = i; j < last; j++) {
            if (userGuesses[i] < userGuesses[j]) {
              //Swapping two values
              int temp = userGuesses[i];
              userGuesses[i] = userGuesses[j];
              userGuesses[j] = temp;
            }
          }
        //Printing homework1.numbers
        for (int i = 0; i < last; i++) {
          System.out.print(" " + userGuesses[i]);
        }
        System.out.println();
        //Generating new answer
        answer = rand.nextInt(100);
        //Clearing the userGuess array
        last = 0;
        //Starting over
        System.out.println("Let's do one more round");
      }
    }
  }
}
