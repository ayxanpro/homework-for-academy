package homework8;

import java.util.Set;

public class RoboCat extends Pet {
  public RoboCat(String nickname) {
    this(nickname, 1, 99, null);
  }

  public RoboCat(String nickname, int age, int trickLevel, Set habits) {
    super(nickname, age, trickLevel, habits);
  }

  public RoboCat() {
    this("creatio");
  }

  @Override
  public void respond() {
    System.out.println("bip,bop,bip");
  }
}
