package homework8;

public final class Man extends Human {

  public Man(String name, String surname, int year) {
    this(name, surname, year, 0);
  }

  public Man(String name, String surname, int year, int iq) {
    super(name, surname, year, iq);
    fillSchedule();
  }

  public Man() {
    this("unknown name(", "unknown surname", -1);
  }

  @Override
  public void greetPet() {
    System.out.printf("Hey, %s\n", this.getFamily().getPet());
  }

  public void repairCar() {
    System.out.println("I'm trying to fix this damn car");
  }

}
