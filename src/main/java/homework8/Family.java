package homework8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Family {

  private Human mother;
  private Human father;
  private ArrayList<Human> children;
  private Set<Pet> pet;

  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;

    this.mother.setFamily(this);
    this.father.setFamily(this);

    this.children = new ArrayList<>();
    this.pet = new HashSet<>();
  }

  @Override
  public String toString() {
    return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}",
        mother, father, getChildrenInfo(), getPetsInfo());
  }

  public void addChild(Human child) {
    if (this.findChild(child)) return;
    child.setFamily(this);
    children.add(child);
  }

  public boolean deleteChild(int index) {
    if (index > children.size() || index < 0) return false;
    if (children.size() == 0) return false;
    children.remove(index);
    return true;
  }

  public boolean deleteChild(Human child) {
    return children.remove(child);
  }

  public void addPet(Pet pet) {
    this.pet.add(pet);
  }

  public boolean findChild(Human child_) {
    for (Human child : children) {
      if (child.equals(child_)) {
        System.out.println(child);
        return true;
      }
    }
    return false;
  }

  public int countFamily() {
    return 2 + children.size();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Family)) return false;
    Family b = (Family) obj;

    if (b.father.equals(this.father) &&
        b.mother.equals(this.mother)) return true;
    return false;
  }

  private String getChildrenInfo() {
    String res = "";
    for (Human child : children) {
      res += child.toString();
    }
    return res == "" ? "no children" : res;
  }

  public String getPetsInfo() {
    String res = "";
    for (Pet p : pet) {
      res += p.toString();
    }
    return res == "" ? "no children" : res;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public ArrayList<Human> getChildren() {
    return children;
  }

  public void setChildren(ArrayList<Human> children) {
    this.children = children;
  }

  public Set<Pet> getPet() {
    return pet;
  }

  public void setPet(Set<Pet> pet) {
    this.pet = pet;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Family has collapsed!!!");
  }
}
