package homework8;

public interface HumanCreator {
  Human bornChild(Human father);

  String[] male_names = {
      "James",
      "Mason",
      "Oliver",
      "Daniel",
      "Lucas",
      "Logan",
      "Jacob",
      "Jackson",
      "Sebastian"
  };
  String[] female_names = {
      "Emma",
      "Olivia",
      "Ava",
      "Isabella",
      "Sophia",
      "Charlotte",
      "Mia"
  };

  static String getRandomName(int gender) {
    if (gender == 0) {
      return male_names[(int) (Math.random() * male_names.length)];
    }
    return female_names[(int) (Math.random() * female_names.length)];
  }
}
