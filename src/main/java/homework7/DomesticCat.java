package homework7;

public class DomesticCat extends Pet implements Foulable {
  public DomesticCat(String nickname) {
    this(nickname, 1, 99, null);
  }

  public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
    super(nickname, age, trickLevel, habits);
  }

  public DomesticCat() {
    this("creatio");
  }

  @Override
  public void foul() {
    System.out.println("I need to cover it up");
  }

  @Override
  public void respond() {
    System.out.println("meow,meow");
  }
}
