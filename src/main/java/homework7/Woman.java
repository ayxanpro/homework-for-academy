package homework7;

import java.time.LocalDate;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {
  public Woman(String name, String surname, int year) {
    this(name, surname, year, 0);
  }

  public Woman(String name, String surname, int year, int iq) {
    super(name, surname, year, iq);
    fillSchedule();
  }

  public Woman() {
    this("unknown name(", "unknown surname", -1);
  }

  @Override
  public void greetPet() {
    System.out.printf("owww, %s\n", this.getFamily().getPet());
  }

  public void makeup() {
    System.out.println("I putting on some makeup^-^");
  }

  public Human bornChild(Human father) {
    Human res;
    Random r = new Random();

    int iq = (this.getIq() + father.getIq()) / 2 + (r.nextInt(10) - 5);
    int gender = r.nextDouble() > 0.5 ? 0 : 1;

    int year = LocalDate.now().getYear();
    String surname = father.getSurname();
    String name = HumanCreator.getRandomName(gender);
    res = gender == 0 ? new Man(name, surname, year, iq) : new Woman(name, surname, year, iq);
    return res;
  }
}
