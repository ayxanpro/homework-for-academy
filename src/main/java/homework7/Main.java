package homework7;

public class Main {
  public static void main(String[] args) {
    Human jen = new Woman("Jennifer", "Deamonid", 2002, 88);
    Human an = new Man("Anglen", "Deamonid", 2000, 89);
    Pet voody = new Dog("voody");

    Family fam = new Family(jen, an);
    fam.setPet(voody);
    System.out.println(fam);

    //Common methods overridden
    jen.greetPet();
    an.greetPet();
    //individual methods
    ((Woman) jen).makeup();
    ((Man) an).repairCar();

    fam.addChild(((Woman) jen).bornChild(an));
    System.out.println(fam);

    fam.addChild(((Woman) jen).bornChild(an));
    System.out.println(fam);
  }
}
