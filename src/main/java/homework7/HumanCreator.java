package homework7;

public interface HumanCreator {
  public Human bornChild(Human father);

  public static String[] male_names = {
      "James",
      "Mason",
      "Oliver",
      "Daniel",
      "Lucas",
      "Logan",
      "Jacob",
      "Jackson",
      "Sebastian"
  };
  public static String[] female_names = {
      "Emma",
      "Olivia",
      "Ava",
      "Isabella",
      "Sophia",
      "Charlotte",
      "Mia"
  };

  public static String getRandomName(int gender) {
    if (gender == 0) {
      return male_names[(int) (Math.random() * male_names.length)];
    }
    return female_names[(int) (Math.random() * female_names.length)];
  }
}
