package homework7;

import java.util.Arrays;

public class Family {

  private Human mother;
  private Human father;
  private Human[] children;
  private Pet pet;

  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;
    this.mother.setFamily(this);
    this.father.setFamily(this);
    this.children = new Human[0];
  }

  @Override
  public String toString() {
    return String.format("Family{mother=%s, father=%s, children=%s, pet=%s}",
        mother, father, getChildrenInfo(), pet);
  }

  public void addChild(Human child) {
    if (this.findChild(child)) return;
    child.setFamily(this);
    children = Arrays.copyOf(children, children.length + 1);
    children[children.length - 1] = child;
  }

  public boolean deleteChild(int index) {
    if (index > children.length || index < 0) return false;
    if (children.length == 0) return false;
    if (children.length == 1) {
      children = new Human[0];
      return true;
    }
    Human temp[] = new Human[children.length - 1];
    int counter = 0;
    children[index].setFamily(null);
    for (int i = 0; i < children.length; i++) {
      if (i != index)
        temp[counter++] = children[i];
    }
    children = temp;
    return true;
  }

  public boolean deleteChild(Human child) {
    if (!findChild(child)) return false;
    if (children.length == 0) return false;
    child.setFamily(null);
    if (children.length == 1) {
      children = new Human[0];
      return true;
    }
    Human[] temp = new Human[children.length - 1];
    //System.out.println("Child to delete " + child);
    int counter = 0;
    for (Human c : children) {
      if (!c.equals(child))
        temp[counter++] = c;
    }
    children = temp;
    return true;
  }

  public boolean findChild(Human child_) {
    for (Human child : children) {
      if (child.equals(child_)) {
        System.out.println(child);
        return true;
      }
    }
    return false;
  }

  public int countFamily() {
    return 2 + children.length;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Family)) return false;
    Family b = (Family) obj;

    if (b.father.equals(this.father) &&
        b.mother.equals(this.mother)) return true;
    return false;
  }

  private String getChildrenInfo() {
    String res = "";
    for (Human child : children) {
      res += child.toString();
    }
    return res == "" ? "no children" : res;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Human[] getChildren() {
    return children;
  }

  public void setChildren(Human[] child) {
    this.children = child;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Family has collapsed!!!");
  }
}
