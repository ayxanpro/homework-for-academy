package homework7;

import java.util.Arrays;

public abstract class Pet {
  private Species species;
  private String nickname;
  private int age;
  private int trickLevel;


  private String[] habits;

  public Pet(String nickname) {
    this(nickname, 1, 99, null);
  }

  public Pet(String nickname, int age, int trickLevel, String[] habits) {
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
    switch (this.getClass().getSimpleName()) {
      case "Fish":
        this.species = Species.FISH;
        break;
      case "DomesticCat":
        this.species = Species.DOMESTICCAT;
        break;
      case "Dog":
        this.species = Species.DOG;
        break;
      case "RoboCat":
        this.species = Species.ROBOCAT;
        break;
      default:
        this.species = Species.UNKNOWN;
    }
  }

  public Pet() {
    this("creatio");
  }

  public void eat() {
    System.out.println("I am eating");
  }

  public abstract void respond();


  @Override
  public String toString() {
    return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
        species, nickname, age, trickLevel,
        (habits != null ? Arrays.toString(habits) : "no habits"));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof Pet)) return false;

    Pet that = (Pet) obj;

    if (this.species != that.getSpecies()) return false;
    if (this.nickname != that.getNickname()) return false;
    if (this.age != that.getAge()) return false;
    if (this.trickLevel != that.getTrickLevel()) return false;

    return true;
  }

  public String isHeSly() {
    return this.trickLevel > 50 ? "very sly"
        : "not so sly";
  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    this.trickLevel = trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void setHabits(String[] habits) {
    this.habits = habits;
  }

  @Override
  protected void finalize() throws Throwable {
    System.out.println("Good bye owner, I'm gone");
  }
}
